import numpy as np
import torch
import random

class Memory:
    def __init__(self, maxsize):
        self.data = []
        self.maxsize = maxsize
        self.pos = 0

    def push(self, data):
        if len(self.data) < self.maxsize:
            self.data.append(None)
        self.data[self.pos] = data
        self.pos = (self.pos + 1) % self.maxsize

    def sample(self, batch_size):
        return random.sample(self.data, batch_size)

    def sample_trace(self):
        nenvs = self.data[0][0].size()[1]
        rands = torch.randint(len(self.data), (nenvs,), dtype=torch.int32)
        data = [tuple(t[:, i] for t in self.data[r]) for i, r in enumerate(rands)]
        data = tuple(map(lambda x: torch.stack(x).transpose(0, 1), zip(*data)))
        return data

def wrap_state(st):
    st = np.ascontiguousarray(st, dtype=np.float32)
    return torch.from_numpy(st).unsqueeze(0)

PAD = 0
UNK = 1
BOS = 2
EOS = 3

PAD_WORD = '<blank>'
UNK_WORD = '<unk>'
BOS_WORD = '<s>'
EOS_WORD = '</s>'
