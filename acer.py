import gym
import torch
import torch.nn.functional as F
import itertools, utils
import numpy as np
from multiprocessing import Pipe, Process
import copy

D_in, H, D_out = 4, 128, 2
epochs = 1000000
gamma = 0.99
q_loss_weight = 0.5
lr = 7e-4
num_workers = 5
num_steps = 10
max_grad_norm = 10
alpha = 0.99
delta = 1
replay_start = 200
replay_steps = 4
c = 10
maxsize = 50000


class Model(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.layer1 = torch.nn.Linear(D_in, H)
        self.action_layer = torch.nn.Linear(H, D_out)
        self.q_layer = torch.nn.Linear(H, D_out)

    def forward(self, input):
        x = F.relu(self.layer1(input))
        actions = self.action_layer(x)
        q = self.q_layer(x)

        return torch.distributions.Categorical(logits=actions), q


class CloudpickleWrapper:
    def __init__(self, x):
        self.x = x

    def __getstate__(self):
        import cloudpickle
        return cloudpickle.dumps(self.x)

    def __setstate__(self, state):
        import pickle
        self.x = pickle.loads(state)


def run_worker(remote, parent_remote, env_fn_wrapper):
    parent_remote.close()
    env = env_fn_wrapper.x()
    try:
        while True:
            cmd, data = remote.recv()
            if cmd == 'step':
                ob, reward, done, info = env.step(data)
                if done:
                    ob = env.reset()
                remote.send((ob, reward, done, info))
            elif cmd == 'reset':
                ob = env.reset()
                remote.send(ob)
            elif cmd == 'close':
                remote.close()
                break
            elif cmd == 'get_spaces':
                remote.send((env.observation_space, env.action_space))
            else:
                raise NotImplementedError
    except KeyboardInterrupt:
        print('SubprocVecEnv worker: got KeyboardInterrupt')
    finally:
        env.close()


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

model = Model().to(device)
average_model = copy.deepcopy(model)
memory = utils.Memory(maxsize)

optimizer = torch.optim.Adam(model.parameters(), lr=lr)
eps = 1e-6

remotes = []
for i in range(num_workers):
    remote, worker_remote = Pipe()
    p = Process(target=run_worker, args=(worker_remote, remote, CloudpickleWrapper(lambda: gym.make("CartPole-v1"))))
    p.daemon = True
    p.start()

    worker_remote.close()
    remotes.append(remote)

remotes[0].send(('get_spaces', None))
observation_space, action_space = remotes[0].recv()

iter_counters = [0] * num_workers
episodes = 0


def broadcast_remotes(type, args=None):
    if args is None:
        args = [None] * num_workers

    for remote, arg in zip(remotes, args):
        remote.send((type, arg))
    return [remote.recv() for remote in remotes]


def train():
    states, log_probs, actions, rewards, dones = memory.sample_trace()
    N = rewards.shape[0]

    dist, q = model(states)

    p = torch.gather(dist.probs[:-1], 2, actions.unsqueeze(2)).squeeze()
    qa = torch.gather(q[:-1], 2, actions.unsqueeze(2)).squeeze()

    rho = p / (torch.exp(log_probs) + eps)
    rho_bar = torch.clamp(rho, max=c)

    v = (dist.probs * q).sum(2)

    qrets = []
    qret = v[-1]
    for i in range(N - 1, -1, -1):
        qret = (rewards[i] + gamma * qret) * (1.0 - dones[i])
        qrets.append(qret)
        qret = rho_bar[i] * (qret - qa[i]) + v[i]

    qrets = torch.stack(qrets[::-1])
    q_loss = (qrets.detach() - qa).pow(2)
    q_loss = q_loss.mean() * q_loss_weight

    q, v = q[:-1], v[:-1]

    policy_loss = rho_bar.detach() * torch.log(eps + p) * (qrets - v).detach()
    advantage_bc = q - v.unsqueeze(-1)
    policy_loss += (torch.clamp(1 - c / (rho + eps), min=0).unsqueeze(-1).detach() * dist.logits[:-1] * advantage_bc.detach()).sum(-1)
    policy_loss = policy_loss.mean()

    g = torch.autograd.grad(-policy_loss, dist.logits, retain_graph=True)[0]
    k = average_model(states)[0].probs / (dist.probs + eps)
    g = g - max(0, ((k * g).sum() - delta) / k.norm(p=2).pow(2)) * k
    g[-1] = 0

    optimizer.zero_grad()
    losses = (dist.logits * g).sum() + q_loss
    losses.backward()
    torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)
    optimizer.step()

    for a_param, param in zip(average_model.parameters(), model.parameters()):
        a_param.data.copy_(alpha * a_param.data + (1 - alpha) * param.data)


states = torch.tensor(broadcast_remotes('reset'), dtype=torch.float).to(device)

for epoch in range(epochs):
    b_data = [[] for _ in range(5)]

    for i in range(num_steps):
        dist, _ = model(states)
        actions = dist.sample()

        data = broadcast_remotes('step', [a.item() for a in actions.cpu().numpy()])
        obs, rewards, dones, _ = zip(*data)
        obs, rewards, dones = map(lambda x: torch.tensor(x, dtype=torch.float).to(device), (obs, rewards, dones))

        for i, done in enumerate(dones):
            if done:
                print("episode: %s iters: %s" % (episodes, iter_counters[i]))
                iter_counters[i] = 0
                episodes += 1
            else:
                iter_counters[i] += 1

        b_data[0].append(states)
        b_data[1].append(dist.log_prob(actions).detach())
        b_data[2].append(actions)
        b_data[3].append(rewards)
        b_data[4].append(dones)

        states = obs

    b_data[0].append(states)
    b_data = tuple(map(lambda x: torch.stack(x), b_data))
    memory.push(b_data)

    if len(memory.data) >= replay_start:
        for _ in range(np.random.poisson(replay_steps)):
            train()
