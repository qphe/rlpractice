import glob
import os
import string
import torch
import unicodedata
import itertools, random
import visdom

lr = 1e-4
hidden_size = 128
clip = 50

log_freq = 1000
save_freq = 10000
confusion_iters = 10000
n_predictions = 3

save_directory = os.path.join("saved_models", "char_rnn")
save_file = os.path.join(save_directory, "320000.tar")

letters = string.ascii_letters + " .,;'"
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def asciify(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in letters
    )


categories = []
category_lines = {}
for filename in glob.glob("data/data/names/*.txt"):
    category = os.path.splitext(os.path.basename(filename))[0]
    categories.append(category)

    with open(filename, encoding='utf-8') as f:
        lines = f.readlines()
    category_lines[category] = [asciify(line) for line in lines]


class RNN(torch.nn.Module):
    DEFAULT, LSTM, GRU = range(3)

    def __init__(self, type, input_size, hidden_size, output_size):
        super(RNN, self).__init__()
        self.type = type

        combined_size = input_size + hidden_size
        self.i2o = torch.nn.Linear(combined_size, output_size)
        self.softmax = torch.nn.LogSoftmax(dim=1)

        if type == RNN.DEFAULT:
            self.i2h = torch.nn.Linear(combined_size, hidden_size)
        elif type == RNN.LSTM:
            self.forget_gate = torch.nn.Linear(combined_size, hidden_size)
            self.input_gate = torch.nn.Linear(combined_size, hidden_size)
            self.output_gate = torch.nn.Linear(combined_size, hidden_size)
            self.context_gate = torch.nn.Linear(combined_size, hidden_size)
        elif type == RNN.GRU:
            self.update_gate = torch.nn.Linear(combined_size, hidden_size)
            self.reset_gate = torch.nn.Linear(combined_size, hidden_size)
            self.recurrent = torch.nn.Linear(combined_size, hidden_size)

    def forward(self, letters, hidden):
        combined = torch.cat((letters, hidden), 1)
        if self.type == RNN.DEFAULT:
            hidden = self.i2h(combined)
        elif self.type == RNN.LSTM:
            f = torch.sigmoid(self.forget_gate(combined))
            i = torch.sigmoid(self.input_gate(combined))
            o = torch.sigmoid(self.output_gate(combined))
            self.context = f * self.context + i * torch.tanh(self.context_gate(combined))
            hidden = o * self.context
        elif self.type == RNN.GRU:
            z = torch.sigmoid(self.update_gate(combined))
            r = torch.sigmoid(self.reset_gate(combined))
            gated = torch.cat((letters, r * hidden), 1)
            hidden = z * hidden + (1 - z) * torch.sigmoid(self.recurrent(gated))
        output = self.i2o(combined)
        return self.softmax(output), hidden

    def evaluate(self, letters):
        hidden = torch.zeros(1, hidden_size).to(device)
        if self.type == RNN.LSTM:
            self.context = torch.zeros(hidden.size()).to(device)

        for i in range(letters.size()[0]):
            output, hidden = self.forward(letters[i], hidden)

        return output


def line_to_index(line):
    input = torch.zeros(len(line), 1, len(letters)).to(device)
    for i, c in enumerate(line):
        input[i][0][letters.find(c)] = 1
    return input


def sample():
    category_index = random.randint(0, len(categories) - 1)
    category = categories[category_index]
    line = random.choice(category_lines[category])
    return line_to_index(line), category_index


rnn = RNN(RNN.GRU, len(letters), hidden_size, len(categories)).to(device)
optimizer = torch.optim.Adam(rnn.parameters(), lr=lr)
loss_func = torch.nn.NLLLoss()


def train():
    current_loss = 0
    for iter in itertools.count():
        letters, category_index = sample()

        target = torch.tensor([category_index], dtype=torch.long).to(device)

        optimizer.zero_grad()

        output = rnn.evaluate(letters)
        loss = loss_func(output, target)
        loss.backward()

        torch.nn.utils.clip_grad_norm_(rnn.parameters(), clip)

        optimizer.step()

        current_loss += loss.item()

        if not iter % log_freq:
            print("iter: %s loss: %s" % (iter, current_loss / log_freq))
            current_loss = 0

        if not iter % save_freq:
            if not os.path.exists(save_directory):
                os.makedirs(save_directory)

            torch.save(rnn.state_dict(), os.path.join(save_directory, '%s.tar' % iter))


def confusion():
    rnn.load_state_dict(torch.load(save_file))
    confusion_matrix = torch.zeros(len(categories), len(categories))

    for i in range(confusion_iters):
        letters, category_index = sample()

        output = rnn.evaluate(letters)
        _, topi = output.topk(1)
        output_category_index = topi[0].item()

        confusion_matrix[category_index][output_category_index] += 1

    vis = visdom.Visdom()
    vis.heatmap(
        X=confusion_matrix,
        opts=dict(
            columnnames=categories,
            rownames=categories
        ))


def predict():
    rnn.load_state_dict(torch.load(save_file))

    while True:
        line = input("> ")
        if line == 'q' or line == 'quit': break

        letters = line_to_index(line)
        output = rnn.evaluate(letters)
        topv, topi = output.topk(n_predictions, 1, True)

        for i in range(n_predictions):
            value = topv[0][i].item()
            index = topi[0][i].item()
            print('(%.2f) %s' % (value, categories[index]))


train()
#confusion()
#predict()
