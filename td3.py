import torch
import gym
import torch.nn.functional as F
import collections, itertools
import utils

H, D_out = 400, 1
actor_lr = 1e-4
critic_lr = 1e-3
epochs = 600
stddev = 0.01
memsize = 1000000
batch_size = 32
gamma = 0.999
tau = 1e-3
eps_stddev = 0.01
c = 0.1
actor_train_freq = 2


class Model(torch.nn.Module):
    def __init__(self, input_size, is_actor=False):
        super().__init__()
        self.layer1 = torch.nn.Linear(input_size, H)
        self.layer2 = torch.nn.Linear(H, H)
        self.layer3 = torch.nn.Linear(H, D_out)
        self.layer3_2 = torch.nn.Linear(H, D_out)
        self.is_actor = is_actor

    def forward(self, input):
        x = F.relu(self.layer1(input))
        x = F.relu(self.layer2(x))
        o = self.layer3(x)
        return o.clamp(-1, 1) if self.is_actor else (o, self.layer3_2(x))

    def critique(self, states, actions):
        return self.forward(torch.cat((states, actions), dim=1))

    def evolve_target(self, model):
        for tp, p in zip(self.parameters(), model.parameters()):
            tp.data.copy_(tau * p.data + (1 - tau) * tp.data)


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
Networks = collections.namedtuple("Networks", ['actor', 'critic'])


def create_networks():
    return Networks(
        Model(3, True).to(device),
        Model(4, False).to(device))


cur_networks, target_networks = create_networks(), create_networks()
for cur_n, target_n in zip(cur_networks, target_networks):
    target_n.load_state_dict(cur_n.state_dict())

actor_optimizer = torch.optim.Adam(cur_networks.actor.parameters(), lr=actor_lr)
critic_optimizer = torch.optim.Adam(cur_networks.critic.parameters(), lr=critic_lr)
mem = utils.Memory(memsize)


def train(iter):
    if len(mem.data) < batch_size:
        return

    batch = mem.sample(batch_size)
    states, actions, rewards, dones, nstates = zip(*batch)
    states, actions, rewards = map(lambda x: torch.cat(x), (states, actions, rewards))

    mask = torch.tensor([not d for d in dones], dtype=torch.uint8).to(device)
    nstates = [nstate for done, nstate in zip(dones, nstates) if not done]
    if not nstates:
        return

    nstates = torch.cat(nstates)
    ys = rewards.clone()
    eps = torch.normal(torch.zeros(nstates.size()[0], 1).to(device), eps_stddev)
    eps = eps.clamp(-c, c)
    target_actions = (target_networks.actor(nstates) + eps).clamp(-1, 1)

    q1, q2 = target_networks.critic.critique(nstates, target_actions)
    ys[mask] += gamma * torch.min(q1, q2)

    cq1, cq2 = cur_networks.critic.critique(states, actions)
    loss = F.mse_loss(ys.detach(), cq1) + F.mse_loss(ys.detach(), cq2)
    critic_optimizer.zero_grad()
    loss.backward()
    critic_optimizer.step()

    if not iter % actor_train_freq:
        loss = -cur_networks.critic.critique(states, cur_networks.actor(states))[0].mean()
        actor_optimizer.zero_grad()
        loss.backward()
        actor_optimizer.step()

        target_networks.actor.evolve_target(cur_networks.actor)
        target_networks.critic.evolve_target(cur_networks.critic)


env = gym.make("Pendulum-v0")
for epoch in range(epochs):
    state = utils.wrap_state(env.reset()).to(device)

    reward = 0
    for iter in itertools.count():
        action = cur_networks.actor(state)
        action += torch.normal(0, std=torch.tensor([stddev]).cuda())
        action = action.detach()

        nstate, r, d, _ = env.step(action.cpu().numpy())
        nstate = utils.wrap_state(nstate.flatten()).to(device)

        mem.push((state, action, torch.tensor([r]).to(device), d, nstate))
        state = nstate

        train(iter)

        reward += r
        if d:
            print("epoch: %s reward: %s iter: %s" % (epoch, reward, iter))
            break

