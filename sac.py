import torch
import gym
import torch.nn.functional as F
import collections, itertools
import utils

H, D_out = 100, 1
lr = 3e-3
epochs = 600
stddev = 0.01
memsize = 1000000
batch_size = 32
gamma = 0.999
tau = 1e-3
eps_stddev = 0.01
c = 0.1
actor_train_freq = 2
log_std_min, log_std_max = -20, 2
alpha = 0.2


class Model(torch.nn.Module):
    ACTOR = 1
    CRITIC = 2
    VALUE = 3

    def __init__(self, input_size, type):
        super().__init__()
        self.type = type
        self.layer1 = torch.nn.Linear(input_size, H)
        self.layer2 = torch.nn.Linear(H, H)
        self.layer3 = torch.nn.Linear(H, D_out)
        self.layer3_2 = torch.nn.Linear(H, D_out)

    def forward(self, input):
        x = F.relu(self.layer1(input))
        x = F.relu(self.layer2(x))
        o = self.layer3(x)

        if self.type == Model.ACTOR:
            std = torch.exp(self.layer3_2(x).clamp(log_std_min, log_std_max))
            e = torch.randn(o.size()).to(device)
            a = o + std * e
            return a, torch.distributions.Normal(o, std).log_prob(a)
        elif self.type == Model.CRITIC:
            return o, self.layer3_2(x)
        elif self.type == Model.VALUE:
            return o

    def critique(self, states, actions):
        return self.forward(torch.cat((states, actions), dim=1))

    def evolve_target(self, model):
        for tp, p in zip(self.parameters(), model.parameters()):
            tp.data.copy_(tau * p.data + (1 - tau) * tp.data)


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
Networks = collections.namedtuple("Networks", ['actor', 'critic', 'value'])


def create_networks():
    return Networks(
        Model(3, Model.ACTOR).to(device),
        Model(4, Model.CRITIC).to(device),
        Model(3, Model.VALUE).to(device))


cur_networks, target_networks = create_networks(), create_networks()
for cur_n, target_n in zip(cur_networks, target_networks):
    target_n.load_state_dict(cur_n.state_dict())

actor_optimizer = torch.optim.Adam(cur_networks.actor.parameters(), lr=lr)
critic_optimizer = torch.optim.Adam(cur_networks.critic.parameters(), lr=lr)
value_optimizer = torch.optim.Adam(cur_networks.value.parameters(), lr=lr)
mem = utils.Memory(memsize)


def train():
    if len(mem.data) < batch_size:
        return

    batch = mem.sample(batch_size)
    states, actions, a_log_probs, rewards, dones, nstates = zip(*batch)
    states, actions, a_log_probs, rewards = map(lambda x: torch.cat(x), (states, actions, a_log_probs, rewards))

    mask = torch.tensor([not d for d in dones], dtype=torch.uint8).to(device)
    nstates = [nstate for done, nstate in zip(dones, nstates) if not done]
    if not nstates:
        return

    nstates = torch.cat(nstates)

    q1, q2 = cur_networks.critic.critique(states, actions)
    q_states = torch.min(q1, q2)
    v_target = q_states - alpha * a_log_probs
    loss = F.mse_loss(cur_networks.value(states), v_target.detach())

    q_target = rewards.clone()
    q_target[mask] += gamma * target_networks.value(nstates)
    loss += F.mse_loss(q1, q_target.detach()) + F.mse_loss(q2, q_target.detach())

    critic_optimizer.zero_grad()
    value_optimizer.zero_grad()
    loss.backward()
    critic_optimizer.step()
    value_optimizer.step()

    sampled_actions, log_pi = cur_networks.actor(states)
    q, _ = cur_networks.critic.critique(states, sampled_actions)
    loss = (alpha * log_pi - q).mean()

    actor_optimizer.zero_grad()
    loss.backward()
    actor_optimizer.step()

    target_networks.value.evolve_target(cur_networks.value)


env = gym.make("Pendulum-v0")
for epoch in range(epochs):
    state = utils.wrap_state(env.reset()).to(device)

    reward = 0
    for iter in itertools.count():
        action, a_log_prob = cur_networks.actor(state)

        nstate, r, d, _ = env.step(action.detach().cpu().numpy())
        nstate = utils.wrap_state(nstate.flatten()).to(device)

        mem.push((state, action.detach(), a_log_prob, torch.tensor([r]).to(device), d, nstate))
        state = nstate

        train()

        reward += r
        if d:
            print("epoch: %s reward: %s iter: %s" % (epoch, reward, iter))
            break
