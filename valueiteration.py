import gym
import scipy

env = gym.make('FrozenLake-v0')

rows, cols = 4, 4

value = [0.0] * (rows * cols)
nvalue = [0.0] * (rows * cols)

discount = 0.99
eps = 0.0001

while True:
    for s in range(len(value)):
        nvalue[s] = 0
        for a in range(4):
            for p, s_prime, r, done in env.env.P[s][a]:
                nvalue[s] = max(nvalue[s], p * (r + discount * value[s_prime]))

    value, nvalue = nvalue, value
    if all(abs(v - nv) < eps for v, nv in zip(value, nvalue)):
        break

for i in range(rows):
    print(value[i * cols: (i + 1) * cols])
