import gym
import itertools
import torch
import utils
import torch.nn.functional as F
import numpy as np

stddev = 0.01
episodes = 600
memsize = 1000000
gamma = 0.999
H, D_out = 400, 1
batch_size = 32
actor_lr = 1e-4
critic_lr = 1e-3
tau = 1e-3

env = gym.make("Pendulum-v0")

class Model(torch.nn.Module):
    def __init__(self, input_size, clamp=False):
        super(Model, self).__init__()
        self.clamp = clamp
        self.layer1 = torch.nn.Linear(input_size, H)
        self.layer2 = torch.nn.Linear(H, H)
        self.layer3 = torch.nn.Linear(H, D_out)

    def forward(self, input):
        x = F.relu(self.layer1(input))
        x = F.relu(self.layer2(x))
        x = self.layer3(x)
        return x.clamp(-1, 1) if self.clamp else x

    def critique(self, state, actions):
        return self.forward(torch.cat((state, actions), dim=1))

    def evolve_target(self, model):
        for tp, p in zip(self.parameters(), model.parameters()):
            tp.data.copy_(tau * p.data + (1 - tau) * tp.data)


mem = utils.Memory(memsize)
actor, critic = Model(3, True).cuda(), Model(4).cuda()
t_actor, t_critic = Model(3, True).cuda(), Model(4).cuda()
actor_optimizer = torch.optim.Adam(actor.parameters(), actor_lr)
critic_optimizer = torch.optim.Adam(critic.parameters(), critic_lr)

def train():
    if len(mem.data) < batch_size:
        return

    samples = mem.sample(batch_size)
    states, acts, rs, ds, nstates = zip(*samples)
    states, acts, rs = map(lambda x: torch.cat(x), (states, acts, rs))

    mask = torch.tensor([not d for d in ds], dtype=torch.uint8).cuda()
    nstates = [nstate for d, nstate in zip(ds, nstates) if not d]
    if not nstates:
        return

    nstates = torch.cat(nstates)

    with torch.no_grad():
        t_q = t_critic.critique(nstates, t_actor(nstates))

        y = rs.clone()
        y[mask] += gamma * t_q

    q = critic.critique(states, acts)
    loss = F.mse_loss(y, q)
    critic_optimizer.zero_grad()
    loss.backward()
    critic_optimizer.step()

    q = -critic.critique(states, actor(states))
    actor_optimizer.zero_grad()
    torch.mean(q).backward()
    actor_optimizer.step()

    t_actor.evolve_target(actor)
    t_critic.evolve_target(critic)

for episode in range(episodes):
    print("episode: %s" % episode)
    state = utils.wrap_state(env.reset()).cuda()

    reward = 0
    for iter in itertools.count():
        #env.render()
        with torch.no_grad():
            action = actor(state)
            action += torch.normal(0, std=torch.tensor([stddev]).cuda())

        nstate, r, d, _ = env.step(action.cpu().numpy())
        reward += r
        nstate = utils.wrap_state(nstate.flatten()).cuda()

        mem.push((state, action, torch.tensor([r]).cuda(), d, nstate))

        train()

        state = nstate
        if d:
            break

    print("reward: %s iter: %s" % (reward, iter))
