from collections import Counter
import numpy as np
import torch
from tqdm import tqdm
import utils
import torch.nn.functional as F
import itertools
import time, math, os
import torch.utils.data

train_src = "data/multi30k/train.en.atok"
train_tgt = "data/multi30k/train.de.atok"
valid_src = "data/multi30k/val.en.atok"
valid_tgt = "data/multi30k/val.de.atok"
test_src = "data/multi30k/test.en.atok"
test_tgt = "data/multi30k/test.de.atok"
save_data = "data/multi30k/multi30k.atok.low.pt"
max_len = 50
min_word_count = 5
embedding_dim = 512
position_inner_dim = 512
batch_size = 32 #32
epochs = 1000
clip_gradient = 50
heads = 8
key_dim = 64
value_dim = 64
dropout_ratio = 0.1
n_layers = 3#6
n_beams = 5
n_best = 1
label_smoothing_eps = 0.1

save_directory = os.path.join("saved_models", "chatbot")
save_file = os.path.join(save_directory, "4000.tar")
output_file = os.path.join("data", "multi30k", "pred.txt")

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')


def read_file(filename, add_begin_end_tokens=False):
    sentences = []
    trimmed = 0

    with open(filename) as f:
        for line in f.readlines():
            words = line.lower().split()
            if len(words) > max_len:
                trimmed += 1
            words = words[:max_len]

            if words and add_begin_end_tokens:
                sentences.append([utils.BOS_WORD] + words + [utils.EOS_WORD])
            elif words and not add_begin_end_tokens:
                sentences.append(words)
            else:
                sentences.append([None])

    print('Get %s instances from %s' % (len(sentences), filename))
    if trimmed:
        print('Trimmed %s to max length %s' % (trimmed, max_len))

    return sentences


def load_sentences(src_filename, tgt_filename):
    src_sentences, tgt_sentences = read_file(src_filename, add_begin_end_tokens=True), read_file(tgt_filename)

    if len(src_sentences) != len(tgt_sentences):
        print('The training count is not equal')
        min_length = min(len(src_sentences), len(tgt_sentences))
        src_sentences = src_sentences[:min_length]
        tgt_sentences = tgt_sentences[:min_length]

    filtered = []
    for src, tgt in zip(src_sentences, tgt_sentences):
        if not src or not tgt:
            continue

        filtered.append((src, tgt))

    return tuple(zip(*filtered))


def build_vocab(sentences):
    lookup = {
        utils.BOS_WORD: utils.BOS,
        utils.EOS_WORD: utils.EOS,
        utils.PAD_WORD: utils.PAD,
        utils.UNK_WORD: utils.UNK,
    }

    count = Counter()
    for sentence in sentences:
        for word in sentence:
            count[word] += 1

    ignored = 0
    for word, count in count.items():
        if word in lookup or count <= min_word_count:
            ignored += 1
            continue
        lookup[word] = len(lookup)
    print('Trimmed vocab size = %s with minimum occurence %s and ignored %s'
          % (len(lookup), min_word_count, ignored))

    return lookup


def convert_to_idx(sentences, vocab):
    return [[vocab.get(w, utils.UNK) for w in s] for s in sentences]


def preprocess():
    train_src_sentences, train_tgt_sentences = load_sentences(train_src, train_tgt)
    valid_src_sentences, valid_tgt_sentences = load_sentences(valid_src, valid_tgt)
    test_src_sentences, test_tgt_sentences = load_sentences(test_src, test_tgt)
    src_vocab, tgt_vocab = build_vocab(train_src_sentences), build_vocab(train_tgt_sentences)

    train_src_sentences, valid_src_sentences, test_src_sentences = \
        map(lambda x: convert_to_idx(x, src_vocab), (train_src_sentences, valid_src_sentences, test_src_sentences))
    train_tgt_sentences, valid_tgt_sentences, test_tgt_sentences = \
        map(lambda x: convert_to_idx(x, tgt_vocab), (train_tgt_sentences, valid_tgt_sentences, test_tgt_sentences))

    src_vocab = {idx: word for word, idx in src_vocab.items()}
    tgt_vocab = {idx: word for word, idx in tgt_vocab.items()}

    data = {
        'vocab': {
            'src': src_vocab,
            'tgt': tgt_vocab,
        },
        'train': {
            'src': train_src_sentences,
            'tgt': train_tgt_sentences,
        },
        'valid': {
            'src': valid_src_sentences,
            'tgt': valid_tgt_sentences,
        },
        'test': {
            'src': test_src_sentences,
            'tgt': test_tgt_sentences
        }
    }

    torch.save(data, save_data)


#preprocess()


class Dataset(torch.utils.data.Dataset):
    def __init__(self,  src_sentences, tgt_sentences):
        self.src_sentences = src_sentences
        self.tgt_sentences = tgt_sentences

    def __len__(self):
        return len(self.src_sentences)

    def __getitem__(self, idx):
        if self.tgt_sentences:
            return self.src_sentences[idx], self.tgt_sentences[idx]
        return self.src_sentences[idx]


def get_indices(sentences):
    sentences = itertools.zip_longest(*sentences, fillvalue=utils.PAD)
    sentences = list(zip(*sentences))
    positions = [[p + 1 if w != utils.PAD else 0 for p, w in enumerate(s)] for s in sentences]
    return torch.tensor(sentences, dtype=torch.long), torch.tensor(positions, dtype=torch.long)


def collate(sentences):
    srcs, tgts = zip(*sentences)
    return (*get_indices(srcs), *get_indices(tgts))


def create_data_loader(src_sentences, tgt_sentences=None, collate_fun=collate):
    return torch.utils.data.DataLoader(
        Dataset(src_sentences, tgt_sentences),
        num_workers=2,
        batch_size=batch_size,
        collate_fn=collate_fun,
        shuffle=True)


data = torch.load(save_data)
src_vocab = data['vocab']['src']
tgt_vocab = data['vocab']['tgt']


def position_embedding():
    def dim_embedding(pos):
        return [pos / np.power(10000, 2 * (d // 2) / embedding_dim) for d in range(embedding_dim)]

    table = np.array([dim_embedding(pos) for pos in range(max_len + 1)])
    table[:, 0::2] = np.sin(table[:, 0::2])
    table[:, 1::2] = np.cos(table[:, 1::2])
    table[utils.PAD] = 0
    return torch.nn.Embedding.from_pretrained(torch.tensor(table, dtype=torch.float), freeze=True)


class ScaledDotProductAttention(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.dropout = torch.nn.Dropout(dropout_ratio)
        self.softmax = torch.nn.Softmax(dim=2)

    def forward(self, q, k, mask=None):
        attn = torch.bmm(q, k.transpose(1, 2))
        attn = attn / np.sqrt(key_dim)

        if mask is not None:
            attn = attn.masked_fill(mask, -np.inf)

        attn = self.softmax(attn)
        attn = self.dropout(attn)
        return attn


class MultiHeadAttention(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.w_qs = torch.nn.Linear(embedding_dim, heads * key_dim)
        self.w_ks = torch.nn.Linear(embedding_dim, heads * key_dim)
        self.w_vs = torch.nn.Linear(embedding_dim, heads * value_dim)
        torch.nn.init.normal_(self.w_qs.weight, mean=0, std=np.sqrt(2.0 / (embedding_dim + key_dim)))
        torch.nn.init.normal_(self.w_ks.weight, mean=0, std=np.sqrt(2.0 / (embedding_dim + key_dim)))
        torch.nn.init.normal_(self.w_vs.weight, mean=0, std=np.sqrt(2.0 / (embedding_dim + value_dim)))

        self.attn = ScaledDotProductAttention()
        self.dropout = torch.nn.Dropout(dropout_ratio)
        self.layer_norm = torch.nn.LayerNorm(embedding_dim)
        self.fc = torch.nn.Linear(heads * value_dim, embedding_dim)

    def forward(self, q, k, v, mask=None):
        bsize = q.size()[0]
        qlen, klen, vlen = q.size()[1], k.size()[1], v.size()[1]
        res = q

        q = self.w_qs(q).view(bsize, qlen, heads, key_dim)
        k = self.w_ks(k).view(bsize, klen, heads, key_dim)
        v = self.w_vs(v).view(bsize, vlen, heads, value_dim)

        q = q.permute(2, 0, 1, 3).contiguous().view(-1, qlen, key_dim)
        k = k.permute(2, 0, 1, 3).contiguous().view(-1, klen, key_dim)
        v = v.permute(2, 0, 1, 3).contiguous().view(-1, vlen, value_dim)

        mask = mask.repeat(heads, 1, 1)
        attn = self.attn(q, k, mask=mask)

        output = torch.bmm(attn, v)
        output = output.view(heads, bsize, qlen, value_dim)
        output = output.permute(1, 2, 0, 3).contiguous().view(bsize, qlen, -1)

        output = self.dropout(self.fc(output))
        output = self.layer_norm(output + res)

        return output, attn


class PositionwiseFeedForward(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.w_1 = torch.nn.Conv1d(embedding_dim, position_inner_dim, 1)
        self.w_2 = torch.nn.Conv1d(position_inner_dim, embedding_dim, 1)
        self.layer_norm = torch.nn.LayerNorm(embedding_dim)
        self.dropout = torch.nn.Dropout(dropout_ratio)

    def forward(self, x):
        res = x
        output = x.transpose(1, 2)
        output = self.w_2(F.relu(self.w_1(output)))
        output = output.transpose(1, 2)
        output = self.dropout(output)
        return self.layer_norm(output + res)


class EncoderLayer(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.attn = MultiHeadAttention()
        self.pos = PositionwiseFeedForward()

    def forward(self, input, mask=None, attn_mask=None):
        output, attn = self.attn(input, input, input, mask=attn_mask)
        output = self.pos(output * mask) * mask
        return output, attn


class DecoderLayer(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.self_attn = MultiHeadAttention()
        self.enc_attn = MultiHeadAttention()
        self.pos = PositionwiseFeedForward()

    def forward(self, dec_input, enc_output, mask=None, attn_mask=None, dec_enc_mask=None):
        dec_output, dec_attn = self.self_attn(dec_input, dec_input, dec_input, mask=attn_mask)
        dec_output *= mask

        dec_output, dec_enc_attn = self.enc_attn(dec_output, enc_output, enc_output, mask=dec_enc_mask)
        dec_output *= mask

        dec_output = self.pos(dec_output) * mask
        return dec_output, dec_attn, dec_enc_attn


class Transformer(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.src_embedding = torch.nn.Embedding(len(src_vocab), embedding_dim, padding_idx=utils.PAD)
        self.tgt_embedding = torch.nn.Embedding(len(tgt_vocab), embedding_dim, padding_idx=utils.PAD)
        self.pos_embedding = position_embedding()

        self.encoder_layers = torch.nn.ModuleList([EncoderLayer() for _ in range(n_layers)])
        self.decoder_layers = torch.nn.ModuleList([DecoderLayer() for _ in range(n_layers)])

        self.tgt_word_prj = torch.nn.Linear(embedding_dim, len(tgt_vocab))
        '''
        self.tgt_word_prj.weight = self.tgt_embedding.weight
        self.logit_scale = (embedding_dim ** -0.5)
        '''
        self.logit_scale = 1
        torch.nn.init.xavier_normal_(self.tgt_word_prj.weight)

    def pad_mask(self, seq):
        return seq.ne(utils.PAD).type(torch.float).unsqueeze(-1)

    def attn_pad_mask(self, seq, qlen=None):
        if not qlen:
            qlen = seq.size()[1]
        return seq.eq(utils.PAD).unsqueeze(1).expand(-1, qlen, -1)

    def subsequent_mask(self, seq):
        ''' For masking out the subsequent info. '''

        bsize , seq_len = seq.size()
        mask = torch.triu(torch.ones((seq_len, seq_len), dtype=torch.uint8).to(device), diagonal=1)
        return mask.unsqueeze(0).expand(bsize, -1, -1)

    def encode(self, src_seq, src_pos):
        enc_pos = self.pos_embedding(src_pos)
        enc_output = self.src_embedding(src_seq) + enc_pos

        mask = self.pad_mask(src_seq)
        attn_mask = self.attn_pad_mask(src_seq)

        for enc_layer in self.encoder_layers:
            enc_output, _ = enc_layer(enc_output, mask=mask, attn_mask=attn_mask)

        return enc_output

    def decode(self, src_seq, enc_output, tgt_seq, tgt_pos):
        mask = self.pad_mask(tgt_seq)
        attn_mask = self.subsequent_mask(tgt_seq)
        attn_mask = (attn_mask + self.attn_pad_mask(tgt_seq)).gt(0)
        dec_enc_mask = self.attn_pad_mask(src_seq, tgt_seq.size()[1])

        dec_output = self.tgt_embedding(tgt_seq) + self.pos_embedding(tgt_pos)

        for dec_layer in self.decoder_layers:
            dec_output, _, _ = dec_layer(
                dec_output,
                enc_output,
                mask=mask,
                attn_mask=attn_mask,
                dec_enc_mask=dec_enc_mask)

        return dec_output

    def forward(self, src_seq, src_pos, tgt_seq, tgt_pos):
        enc_output = self.encode(src_seq, src_pos)
        dec_output = self.decode(src_seq, enc_output, tgt_seq, tgt_pos)
        return self.tgt_word_prj(dec_output) * self.logit_scale


transformer = Transformer().to(device)
parameters = filter(lambda x: x.requires_grad, transformer.parameters())
optimizer = torch.optim.Adam(parameters)


def run(epoch, dataset, is_validation=False):
    start = time.time()
    label = ("Validation" if is_validation else "Training")

    if is_validation:
        transformer.eval()
    else:
        transformer.train()

    total_loss = 0
    n_correct = 0
    n_words = 0
    for batch in tqdm(
            dataset,
            mininterval=2,
            desc='  - (%s)   ' % label,
            leave=False):
        src_seq, src_pos, tgt_seq, tgt_pos = map(lambda x: x.to(device), batch)

        pred = transformer(src_seq, src_pos, tgt_seq, tgt_pos)

        pred = pred.view(-1, pred.size(2))
        ans = tgt_seq.view(-1).contiguous()

        one_hot = torch.zeros(pred.size()).to(device).scatter(1, ans.view(-1, 1), 1)
        one_hot = one_hot * (1 - label_smoothing_eps) + (1 - one_hot) * label_smoothing_eps / (pred.size()[1] - 1)
        log_prb = F.log_softmax(pred, dim=1)

        non_pad_mask = ans.ne(utils.PAD)
        loss = -(one_hot * log_prb).sum(dim=1)
        loss = loss.masked_select(non_pad_mask).sum()

        #loss = F.cross_entropy(pred, ans, ignore_index=utils.PAD, reduction='sum')

        if not is_validation:
            optimizer.zero_grad()
            loss.backward()
            #torch.nn.utils.clip_grad_norm_(parameters, clip_gradient)
            optimizer.step()

        valid_words = ans.ne(utils.PAD)
        total_loss += loss.item()
        pred_idx = pred.max(1)[1]
        n_correct += pred_idx.eq(ans).masked_select(valid_words).sum().item()
        n_words += valid_words.sum().item()

    if not is_validation:
        if not os.path.isdir(save_directory):
            os.makedirs(save_directory)
        torch.save(transformer.state_dict(), os.path.join(save_directory, '%s.tar' % epoch))

    loss, acc = total_loss / n_words, n_correct / n_words
    print(" - (%s)  loss: %f, accuracy: %f elapsed: %s min"
          % (label, math.exp(min(loss, acc)), 100 * acc, (time.time() - start) / 60))


def train():
    train_data = create_data_loader(data['train']['src'], data['train']['tgt'])
    valid_data = create_data_loader(data['valid']['src'], data['valid']['tgt'])

    for epoch in range(epochs):
        run(epoch, valid_data, is_validation=True)
        run(epoch, train_data)


class Beam:
    def __init__(self):
        self.ks = []
        self.ys = []

    def advance(self, prob):
        if self.prob is None:
            next_prob = self.prob + prob
        else:
            next_prob = prob[0]

        next_prob = next_prob.view(-1)

        best_scores, best_scores_id = next_prob.topk(n_beams, 0)

        prev_k = best_scores_id / prob.size(1)
        self.scores = best_scores
        self.ks.append(prev_k)
        self.ys.append(best_scores_id - prev_k * prob.size(1))

        self.done = self.ys[-1][0].item() == utils.EOS
        return self.done

    def tentative_hypothesis(self):
        "Get the decoded sequence for the current timestep."

        if len(self.ys) == 1:
            dec_seq = self.ys[0].unsqueeze(1)
        else:
            _, keys = self.scores.sort(0, True)
            hyps = [self.hypothesis(k) for k in keys]
            hyps = [[utils.BOS] + h for h in hyps]
            dec_seq = torch.tensor(hyps, dtype=torch.long)

        return dec_seq

    def hypothesis(self, k):
        res = []
        for j in range(len(self.ks) - 1, -1, -1):
            res.append(self.ys[j + 1][k])
            k = self.ks[j][k]
        return list(map(lambda x: x.item(), res[::-1]))


def translate():
    transformer.load_state_dict(torch.load(save_file))
    test_data = create_data_loader(data['test']['src'], data['test']['tgt'], collate_fun=get_indices)

    lines = []

    for batch in tqdm(
            test_data,
            mininterval=2,
            desc='  - (Translate)   ',
            leave=False):
        with torch.no_grad():
            src_seq, src_pos = map(lambda x: x.to(device), batch)
            src_enc = transformer.encode(src_seq, src_pos)

            bsize, src_len = src_seq.size()
            src_seq = src_seq.repeat(1, n_beams).view(-1, src_len)
            src_enc = src_enc.repeat(1, n_beams, 1).view(-1, src_len, embedding_dim)

            beams = [Beam() for _ in range(bsize)]

            active_indexes = list(range(bsize))
            index_to_position = {i: index for i, index in enumerate(active_indexes)}

            for dec_len in range(1, max_len):
                dec_seq = [b.tentative_hypothesis() for b in beams if not b.done]
                dec_seq = torch.stack(dec_seq).to(device).view(-1, dec_len)

                dec_pos = torch.arange(1, dec_len + 1, dtype=torch.long).to(device)
                dec_pos = dec_pos.unsqueeze(0).repeat(bsize * n_beams, 1)

                dec_output = transformer.decode(src_seq, src_enc, dec_seq, dec_pos)[:, -1, :]
                prob = F.log_softmax(transformer.tgt_word_prj(dec_output), dim=1).view(bsize, n_beams, -1)

                active_indexes = []
                for index, pos in index_to_position.items():
                    complete = beams[index].advance(prob[pos])
                    if not complete:
                        active_indexes.append(index)

                cur_active, next_active = len(index_to_position), len(active_indexes)

                active_idx = [index_to_position[i] for i in active_indexes]
                active_idx = torch.tensor(active_idx, dtype=torch.long).to(device)

                src_seq = src_seq.view(cur_active, -1).index_select(0, active_idx).view(next_active, -1)
                src_pos = src_pos.view(cur_active, -1).index_select(0, active_idx).view(next_active, -1)
                index_to_position = {i: index for i, index in enumerate(active_indexes)}

        hypotheses, scores = [], []
        for i, beam in enumerate(beams):
            scores, tail_idxs = beam.scores.sort(0, True)
            scores.append([scores[:n_best]])
            hypotheses.append([beam.hypothesis(i) for i in tail_idxs[:n_best]])

        for seq in hypotheses:
            line = ' '.join([tgt_vocab[idx] for idx in seq])
            lines.append(line)
                
    with open(output_file, 'w') as f:
        f.writelines(lines)


train()
#translate()

