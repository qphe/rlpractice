from __future__ import absolute_import, division, unicode_literals

import torch
import os
import csv
import re
import collections
import random
import itertools
import torch.nn.functional as F

MAX_SENTENCE_LENGTH = 10
MIN_WORD_COUNT = 3
batch_size = 64
hidden_size = 500
n_layers = 2
dropout_ratio = 0.1 if n_layers != 1 else 0
encoder_lr = 1e-4
decoder_lr = 5e-4
teacher_forcing_percent = 1.0
clip_gradient = 50
train_log_freq = 1
train_save_freq = 500

root = os.path.join("data", "cornell movie-dialogs corpus")
datafile = os.path.join(root, "formatted_movie_lines.txt")
save_directory = os.path.join("saved_models", "chatbot")
save_file = os.path.join(save_directory, "4000.tar")

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def normalize(line):
    line = line.lower().strip()
    line = re.sub(r"([\\.!?])", r" \1 ", line)
    line = re.sub(r"[^a-zA-Z\\.!?]+", r" ", line)
    return re.sub(r"\s+", r" ", line).strip()


def preprocess_data():
    MOVIE_LINES_FIELDS = ["lineID", "characterID", "movieID", "character", "text"]
    MOVIE_CONVERSATIONS_FIELDS = ["character1ID", "character2ID", "movieID", "utteranceIDs"]

    print("Processing corpus...")
    lines = {}
    with open(os.path.join(root, "movie_lines.txt"), 'r', encoding='iso-8859-1') as f:
        for line in f:
            values = line.split(" +++$+++ ")
            lineObj = {field: value for field, value in zip(MOVIE_LINES_FIELDS, values)}
            lines[lineObj['lineID']] = lineObj

    print("Loading conversations...")
    conversations = []
    with open(os.path.join(root, "movie_conversations.txt"), 'r', encoding='iso-8859-1') as f:
        for line in f:
            values = line.split(" +++$+++ ")
            convObj = {field: value for field, value in zip(MOVIE_CONVERSATIONS_FIELDS, values)}
            convObj["lines"] = [lines[lineId] for lineId in eval(convObj["utteranceIDs"])]
            conversations.append(convObj)

    print("Writing newly formatted file...")
    with open(datafile, 'w', encoding='utf-8') as outputfile:
        writer = csv.writer(outputfile, delimiter='\t', lineterminator="\n")

        pairs = []
        for conversation in conversations:
            lines = []
            for line in conversation["lines"]:
                lines.append(normalize(line['text']))

            for inputLine, targetLine in zip(lines[:-1], lines[1:]):
                if len(inputLine.split(" ")) > MAX_SENTENCE_LENGTH or len(targetLine.split(" ")) > MAX_SENTENCE_LENGTH:
                    continue

                if inputLine and targetLine:
                    pairs.append([inputLine, targetLine])

        word_counter = WordCounter(pairs, filter=True)
        for p in pairs:
            words = p[0].split(" ") + p[1].split(" ")
            skip = False
            for w in words:
                if w not in word_counter.index.keys():
                    skip = True

            if skip:
                continue
            writer.writerow(p)

    print("Sample lines from file:")
    with open(datafile, 'rb') as f:
        lines = f.readlines()
    for line in lines[:10]:
        print(line)


class WordCounter:
    def __init__(self, pairs, filter=False):
        self.words = ["PAD", "SOS", "EOS"]

        seen_words = set()
        counter = collections.Counter(self.words)
        for p in pairs:
            for s in p:
                for word in s.split(" "):
                    counter[word] += 1
                    if word not in seen_words:
                        seen_words.add(word)
                        self.words.append(word)

        self.words = [word for word in self.words if not filter or counter[word] >= MIN_WORD_COUNT]
        self.index = {word: index for index, word in enumerate(self.words)}
        self.counter = {word: counter[word] for word in self.words}


#preprocess_data()
with open(datafile, encoding='utf-8') as f:
    lines = f.readlines()
    pairs = [[s for s in l.split('\t')] for l in lines]
    word_counter = WordCounter(pairs)
embedding = torch.nn.Embedding(len(word_counter.words), hidden_size).to(device)


def process(sentences):
    indexes = [[word_counter.index[word] for word in sentence.split(" ")] for sentence in sentences]
    indexes.sort(key=lambda index: -len(index))
    lengths = [len(index) for index in indexes]
    indexes = list(itertools.zip_longest(*indexes, fillvalue=0))

    mask = torch.tensor([[i != 0 for i in index] for index in indexes], dtype=torch.uint8).to(device)

    lengths = torch.tensor(lengths).to(device)
    return torch.tensor(indexes, dtype=torch.long).to(device), lengths, mask


class Encoder(torch.nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.gru = torch.nn.GRU(hidden_size, hidden_size, n_layers, dropout=dropout_ratio, bidirectional=True)

    def forward(self, seq, lengths, hidden=None):
        embedded = embedding(seq)
        packed = torch.nn.utils.rnn.pack_padded_sequence(embedded, lengths)
        outputs, hidden = self.gru(packed, hidden)
        outputs, _ = torch.nn.utils.rnn.pad_packed_sequence(outputs)
        outputs = outputs[:, :, :hidden_size] + outputs[:, :, hidden_size:]
        return outputs, hidden

class Decoder(torch.nn.Module):
    GENERAL, DOT, CONCAT = range(3)

    def __init__(self, attn_type):
        super(Decoder, self).__init__()
        self.attn_type = attn_type
        self.embedding_dropout = torch.nn.Dropout(dropout_ratio)
        self.gru = torch.nn.GRU(hidden_size, hidden_size, n_layers, dropout=dropout_ratio)

        if attn_type == Decoder.GENERAL:
            self.attn = torch.nn.Linear(hidden_size, hidden_size)
        elif attn_type == Decoder.CONCAT:
            self.attn = torch.nn.Linear(hidden_size * 2, hidden_size)
            self.v = torch.nn.Parameter(torch.tensor(hidden_size, dtype=torch.float32))

        self.concat = torch.nn.Linear(hidden_size * 2, hidden_size)
        self.out = torch.nn.Linear(hidden_size, len(word_counter.words))

    def forward(self, decoder_input, hidden, encoder_output):
        embedded = embedding(decoder_input)
        embedded = self.embedding_dropout(embedded)

        rnn_output, hidden = self.gru(embedded, hidden)

        if self.attn_type == Decoder.DOT:
            attn = torch.sum(rnn_output * encoder_output, dim=2)
        elif self.attn_type == Decoder.GENERAL:
            attn = self.attn(encoder_output)
            attn = torch.sum(rnn_output * attn, dim=2)
        elif self.attn_type == Decoder.CONCAT:
            attn = self.attn(torch.cat((rnn_output.expand(encoder_output.size(0), -1, -1), encoder_output)))
            attn = torch.sum(self.v * attn, dim=2)

        attn = F.softmax(attn.t(), dim=1).unsqueeze(1)
        context = attn.bmm(encoder_output.transpose(0, 1))

        rnn_output = rnn_output.squeeze(0)
        context = context.squeeze(1)

        concat_input = torch.cat((rnn_output, context), 1)
        concat_output = torch.tanh(self.concat(concat_input))

        output = self.out(concat_output)
        return F.softmax(output, dim=1), hidden


encoder = Encoder().to(device)
decoder = Decoder(Decoder.DOT).to(device)

encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=encoder_lr)
decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=decoder_lr)

def train():
    print_loss = 0
    for iter in itertools.count():
        batch = [random.choice(pairs) for _ in range(batch_size)]
        batch = list(zip(*batch))

        input_indexes, input_lengths, _ = process(batch[0])
        target_indexes, target_lengths, target_mask,  = process(batch[1])

        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()

        encoder_outputs, encoder_hidden = encoder(input_indexes, input_lengths)

        decoder_input = torch.tensor([[word_counter.index["SOS"] for _ in range(batch_size)]], dtype=torch.int64).to(device)
        decoder_hidden = encoder_hidden[:n_layers]

        total_loss, accumulated_loss, count = 0, 0, 0
        for t in range(max(target_lengths)):
            decoder_output, decoder_hidden = decoder(decoder_input, decoder_hidden, encoder_outputs)
            target_index = target_indexes[t]
            if random.random() < teacher_forcing_percent:
                decoder_input = target_index.unsqueeze(0)
            else:
                _, topi = decoder_output.topk(1)
                decoder_input = torch.tensor([[topi[i][0] for i in range(batch_size)]], dtype=torch.uint64).to(device)

            likelihood = -torch.log(torch.gather(decoder_output, 1, target_index.view(-1, 1)))
            loss = likelihood[target_mask[t]].sum()
            '''
            print(loss)
            loss = likelihood.masked_select(target_mask[t])
            print(loss)
            '''

            total_loss += loss

            masked = target_mask[t].sum().item()
            accumulated_loss += loss.item() * masked
            count += masked

        total_loss.backward()
        torch.nn.utils.clip_grad_norm_(encoder.parameters(), clip_gradient)
        torch.nn.utils.clip_grad_norm_(decoder.parameters(), clip_gradient)

        encoder_optimizer.step()
        decoder_optimizer.step()

        print_loss += accumulated_loss / count
        if not iter % train_log_freq:
            print("iter: %s loss: %s" % (iter, print_loss))
            print_loss = 0

        if not iter % train_save_freq:
            if not os.path.exists(save_directory):
                os.makedirs(save_directory)

            torch.save({
                'iteration': iter,
                'enc': encoder.state_dict(),
                'dec': decoder.state_dict(),
                'embedding': embedding.state_dict()
            }, os.path.join(save_directory, '%s.tar' % iter))

def run():
    saved = torch.load(save_file)

    encoder.load_state_dict(saved['enc'])
    decoder.load_state_dict(saved['dec'])
    embedding.load_state_dict(saved['embedding'])

    encoder.eval()
    decoder.eval()

    while True:
        sentence = input('> ')
        if sentence == 'q' or sentence == 'quit': break

        sentence = normalize(sentence)
        indexes, lengths, _ = process([sentence])

        encoder_outputs, encoder_hidden = encoder(indexes, lengths)

        decoder_input = torch.tensor([[word_counter.index["SOS"]]], dtype=torch.long).to(device)
        decoder_hidden = encoder_hidden[:n_layers]

        all_tokens = [0]

        for _ in range(MAX_SENTENCE_LENGTH):
            decoder_output, decoder_hidden = decoder(decoder_input, decoder_hidden, encoder_outputs)
            decoder_scores, decoder_input = torch.max(decoder_output, dim=1)

            all_tokens.append(decoder_input.item())
            decoder_input = torch.unsqueeze(decoder_input, 0)

        words = [word_counter.words[token] for token in all_tokens]
        print("Bot: ", ' '.join([word for word in words if word not in {'EOS', 'PAD'}]))


train()
#run()
