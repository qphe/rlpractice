import gym
import torch
import copy
import random, itertools
import utils

N, D_in, H, D_out = 64, 4, 200, 2
lr = 1e-4
memory_size = 1000000
epsilon = 1.0
epsilon_decay = 0.995
epsilon_min = 0.01
gamma = 0.999

epochs = 600
copy_freq = 10

device = torch.device('cuda')


class Model:
    def __init__(self):
        self.model = torch.nn.Sequential(
            torch.nn.Linear(D_in, H),
            torch.nn.ReLU(),
            torch.nn.Linear(H, H),
            torch.nn.ReLU(),
            torch.nn.Linear(H, H),
            torch.nn.ReLU(),
            torch.nn.Linear(H, D_out)
        ).to(device)

        self.optimizer = torch.optim.Adam(self.model.parameters(), lr)


target, cur = Model(), Model()
mem = utils.Memory(memory_size)
env = gym.make("CartPole-v1")


def train():
    if len(mem.data) < N:
        return

    samples = mem.sample(N)
    states, acts, rs, nstates = zip(*samples)
    states, acts, rs = map(lambda x: torch.cat(x), (states, acts, rs))
    pred = cur.model(states).gather(1, acts)

    target_mask = torch.tensor(tuple(map(lambda s: s is not None, nstates)),
                               device=device, dtype=torch.uint8)
    nstates = [s for s in nstates if s is not None]
    if not nstates:
        return

    target_input_states = torch.cat(nstates)
    targets = torch.zeros(N).cuda()
    targets[target_mask] = target.model(target_input_states).max(1)[0].detach()
    targets = targets * gamma + rs

    loss = torch.nn.functional.smooth_l1_loss(pred, targets.unsqueeze(1))

    cur.optimizer.zero_grad()
    loss.backward()
    for param in cur.model.parameters():
        param.grad.data.clamp_(-1, 1)
    cur.optimizer.step()


for epoch in range(epochs):
    print("epoch: %s" % epoch)
    state = utils.wrap_state(env.reset()).cuda()
    for iter in itertools.count():
        #env.render()
        if random.random() < epsilon:
            act = torch.tensor([[random.randrange(2)]], device=device, dtype=torch.long)
        else:
            with torch.no_grad():
                act = target.model(state).max(1)[1].view(1, 1)
        nstate, r, done, _ = env.step(act.item())
        nstate = None if done else utils.wrap_state(nstate).cuda()
        r = torch.tensor([r], device=device)

        mem.push((state, act, r, nstate))
        epsilon = max(epsilon_min, epsilon * epsilon_decay)
        state = nstate

        train()

        if done:
            print("iter: %s" % iter)
            break

    if epoch % copy_freq == 0:
        target.model = copy.deepcopy(cur.model)
