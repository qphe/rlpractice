import torch, gym
import numpy as np
from enum import Enum
import itertools
import torch.nn.functional as F
import utils

class Type(Enum):
    REINFORCE = 1
    ACTOR_CRITIC = 2

N, D_in, H, D_out = 1, 4, 128, 2
lr = 1e-2
episodes = 10000
gamma = 0.999
value_loss_weight = 0.5
entropy_loss_weight = 0.01
type = Type.ACTOR_CRITIC
PPO = True
PPO_eps = 0.2

env = gym.make("CartPole-v1")
device = torch.device("cuda")


class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.layer1 = torch.nn.Linear(D_in, H)
        self.action_layer = torch.nn.Linear(H, 2)
        self.value_layer = torch.nn.Linear(H, 1)

    def forward(self, input):
        x = F.relu(self.layer1(input))
        actions = self.action_layer(x)
        values = self.value_layer(x)

        dist = torch.distributions.Categorical(logits = actions)

        if type == Type.REINFORCE:
            return dist
        elif type == Type.ACTOR_CRITIC:
            return dist, values


model = Model().cuda()
optimizer = torch.optim.Adam(model.parameters(), lr=lr)
eps = np.finfo(np.float32).eps.item()

for episode in range(episodes):
    print("episode: %s" % episode)
    state = utils.wrap_state(env.reset()).cuda()

    rewards = []
    actions = []
    values = []
    deltas = []
    entropies = []

    for iter in itertools.count():
        if type == Type.REINFORCE:
            d = model(state)
        elif type == Type.ACTOR_CRITIC:
            d, value = model(state)
            values.append(value)

        act = d.sample()
        nstate, r, done, _ = env.step(act.item())
        nstate = utils.wrap_state(nstate).cuda()

        actions.append(d.log_prob(act))
        rewards.append(r)

        with torch.no_grad():
            _, nv = model(nstate)
            deltas.append(r + nv * gamma - values[iter])

        entropies.append(d.entropy())

        state = nstate

        if done:
            print("iter: %s" % iter)
            break

    if rewards:
        for i in reversed(range(len(rewards) - 2)):
            rewards[i] += gamma * rewards[i + 1]

        optimizer.zero_grad()
        losses = torch.zeros(1).cuda()

        rewards = torch.tensor(rewards).cuda()
        rewards = (rewards - rewards.mean()) / (rewards.std() + eps)
        for i, (reward, act, entropy) in enumerate(zip(rewards, actions, entropies)):
            if type == Type.REINFORCE:
                losses -= reward * act * (gamma ** (i + 1))
            elif type == Type.ACTOR_CRITIC:
                advantage = deltas[i].view(-1)
                value_loss = (reward - values[i].view(-1)).pow(2)

                if PPO:
                    r_theta = torch.exp(act - act.detach())
                    advantage_loss = torch.min(r_theta * advantage, r_theta.clamp(1 - PPO_eps, 1 + PPO_eps) * advantage)
                    losses += -advantage_loss + value_loss * value_loss_weight - entropy * entropy_loss_weight
                else:
                    losses += -advantage * act * (gamma ** i) + value_loss * value_loss_weight

        losses.backward()
        torch.nn.utils.clip_grad_norm(model.parameters(), 1)
        optimizer.step()


